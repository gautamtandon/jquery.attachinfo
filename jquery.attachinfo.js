/* The MIT License (MIT)

Copyright (c) 2013 Gautam Tandon (gt.2001@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

(function( $ ){
	$.fn.attachInfo = function(options) {
		var settings = $.extend({
			'sampleText': '',
			'description': '',
			'startWith': 'Example: ',
			'showInfoIcon': true,
			'styleName': 'blue'
		}, options);

		if (settings.sampleText != '') {
			this.watermark(settings['startWith']+settings['sampleText']);
		}

		if (settings.description != '') {
			var qTipTgt = this;
			if (settings.showInfoIcon) {
				if (this.parent().find('#es_qtip').length == 0) {
					this.after("<a id='es_qtip' href='#'>&nbsp;<span class='ui-icon ui-icon-info' style='display: inline-block; vertical-align: middle'></span></a>");
				}
				qTipTgt = this.parent().find('#es_qtip').first();
			}
			if (qTipTgt.data('qtip') != null) {
				qTipTgt.qtip('destroy');
			}
			qTipTgt.qtip({
				style: {
					name: settings['styleName'],
					border: {
						width: 1,
						radius: 10
					},
					padding: 5,
				},
				content: {
					text: "<span style='font-family: arial; font-size: 11px; color: black'>"+settings['description']+"</span>"
				},
				position: {
					corner: {
						target: 'center',
						tooltip: 'topLeft'
					}, 
					adjust: { screen: true }
				},
				show: 'mouseover',
				hide: {fixed: true}
			});
		}
	};
})( jQuery );