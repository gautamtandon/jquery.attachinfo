#jquery.attachinfo
jQuery Plugin to attach informational text to any HTML element.

##About
This plugin can be used to attach informational text to any HTML element. It adds an icon just next to the element and shows informational text via a nicely rendered tooltip as soon as you hover over the icon. It also supports "watermark" capability, which can be used in case you'd want to show some example text in your input fields.

Here's how it'll look:
![sample image](http://neverendingthoughts.com/wp-content/uploads/2013/06/jquery-attachinfo.png)

##Installation
This plugin requires following artifacts:

    +------------+-----------------------------------+------------------------------+
    | Artifact   | Files                             | Purpose                      |
    +------------+-----------------------------------+------------------------------+
    | jQuery     | jquery-<version>.min.js           | Well... It is jQuery!        |
    +------------+-----------------------------------+------------------------------+
    | jQuery UI  | jquery-ui-<version>.custom.min.js | Used to render the icon and  |
    |            | jquery-ui-<version>.custom.css    | tooltip.                     |
    +------------+-----------------------------------+------------------------------+
    | qTip       | jquery.qtip-<version>.min.js      | Plugin used to show a "nice" |
    |            |                                   | tooltip.                     |
    +------------+-----------------------------------+------------------------------+
    | watermark  | jquery.watermark.min.js           | Plugin used to show          |
    |            |                                   | "watermark"                  |
    +------------+-----------------------------------+------------------------------+
    | attachinfo | jquery.attachinfo.js              | This plugin!                 |
    +------------+-----------------------------------+------------------------------+


##Example Usage
Here's an example HTML file showing how you can add informational text to an input field.

    <html>
    	<head>
			<!-- Make sure you have JQuery, JQuery-UI and appropriate JQuery Theme -->
    		<script src="jquery-1.6.2.min.js" ></script>
    		<script src="jquery-ui-1.8.16.custom.min.js" ></script>
    		<link rel="stylesheet" type="text/css" href="jquery-ui-1.8.16.custom.css" />
    		
			<!-- Add qtip and watermark plugins -->
    		<script src="jquery.qtip-1.0.0-rc3.min.js" ></script>
    		<script src="jquery.watermark.min.js" ></script>
    		
			<!-- Add the attachinfo plugin -->
    		<script src="jquery.attachinfo.js" ></script>
			
    		<script>
    			$(document).ready(function() {
					// Attach informational text to 'my_input' input field
    				$('#my_input').attachInfo({
    					description: "This is my test input field",
    					sampleText: "Hello World!"
    				});
    			});
    		</script>
    	</head>
    	<body>
    		<h1>jquery.attachinfo - sample test page</h1>
    		Input: <input id="my_input" />
    	</body>
	</html>

##Options
The plugin provides following options:

    +--------------+---------+----------+-------------------------------------------+
    | Option       | Type    | Default  | Purpose                                   |
    +--------------+---------+----------+-------------------------------------------+
    | description  | string  |          | The informational text to be shown in the |
    |              |         |          | tooltip.                                  |
    +--------------+---------+----------+-------------------------------------------+
    | styleName    | string  | blue     | The tooltip style color to be used.       |
    |              |         |          | Possible Values are: cream, dark, green,  |
    |              |         |          | light, red, blue                          |
    +--------------+---------+----------+-------------------------------------------+
    | sampleText   | string  |          | Populate some default text as a           |
    |              |         |          | "watermark" in input fields, etc. If not  |
    |              |         |          | provided, no watermark is shown.          |
    +--------------+---------+----------+-------------------------------------------+
    | startWith    | string  | Example: | When using sampleText, the watermark will |
    |              |         |          | start with this string. You may set it to |
    |              |         |          | "eg: " or make it blank if you do not     |
    |              |         |          | want to show it.                          |
    +--------------+---------+----------+-------------------------------------------+
    | showInfoIcon | boolean | true     | In some cases you might just want to show |
    |              |         |          | some sampleText and no tooltip icon. You  |
    |              |         |          | can do that by simply setting this option |
    |              |         |          | to "false".                               |
    +--------------+---------+----------+-------------------------------------------+

##Reporting Bugs or Enhancements

You can log issues or enhancement requests by clicking [here](https://bitbucket.org/gautamtandon/jquery.attachinfo/issues?status=new&status=open) and I will try to address them as soon as possible.

##License

This utility has been kept open sourced and made available free of cost under the MIT License.

The MIT License (MIT)

Copyright (c) 2013 Gautam Tandon (gt.2001@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.